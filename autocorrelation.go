package WaveformAnalysis

import (
	"log"

	"github.com/montanaflynn/stats"
)

func Mean(wf []float64) float64 {
	//Compute mean of the Waveform Time Serie
	data := stats.LoadRawData(wf)
	v, err := stats.Mean(data)

	if err != nil {
		log.Fatal(err)
	}

	return v
}

func CZero(wf []float64, mean float64) float64 {
	// Compute the C0 Sum (X-mean)**2
	C0 := 0.0
	N := float64(len(wf))
	for _, value := range wf {
		C0 += (float64(value) - mean) * (float64(value) - mean)
	}
	return C0 / N

	// data := stats.LoadRawData(wf)
	// stdev, _ := stats.StandardDeviation(data)
	// log.Println(stdev)
	// return stdev * stdev
}

func Rh(wf *[]float64, i int64, C0 float64, mean float64, res chan float64) {
	// Compute the autocorrelation coefficient Rh
	n := int64(len(*wf))
	nmax := n - i

	Rh := 0.0
	// Loop over the waveform and compute Sum(x[k]-mean)(x[k+i]-mean)
	for k := 0; k < int(nmax); k++ {
		Rh += (float64((*wf)[k]) - mean) * (float64((*wf)[i+int64(k)]) - mean)
	}

	// Normalize and send data to channel
	Rh = Rh / C0
	res <- Rh
}

func Autocorellation(deltat float64, wf *[]float64) ([]float64, []float64) {

	//Constants needed for autocorrelation
	wf_mean := Mean(*wf)
	wf_c0 := CZero(*wf, wf_mean)
	NMax := int(len(*wf) / 2)

	//Channel slice to hold response from Rh goroutines
	var Rhs_chan []chan float64
	for i := 0; i < NMax; i++ {
		Rhs_chan = append(Rhs_chan, make(chan float64))
	}

	Rhs := make([]float64, NMax)
	dts := make([]float64, NMax)

	// Send one go routine per Rh calculation to be done
	for i := 0; i < NMax; i++ {
		go Rh(wf, int64(i+1), wf_c0, wf_mean, Rhs_chan[i])
	}

	// Log the results to the output slices
	for i := 0; i < NMax; i++ {
		Rhs[i] = <-Rhs_chan[i]
		dts[i] = float64(i+1) * deltat
	}

	return dts, Rhs
}

func AutocorellationST(wf *[]float64, deltat float64) ([]float64, []float64) {
	// Same as normal, just single thread
	wf_mean := Mean(*wf)
	wf_c0 := CZero(*wf, wf_mean)
	NMax := int(len(*wf) / 2)

	var Rhs_chan []chan float64

	for i := 0; i < NMax; i++ {
		Rhs_chan = append(Rhs_chan, make(chan float64))
	}
	Rhs := make([]float64, NMax)
	dts := make([]float64, NMax)

	// Here we wait for each routine to end before starting the next one
	for i := 0; i < NMax; i++ {
		go Rh(wf, int64(i), wf_c0, wf_mean, Rhs_chan[i])
		Rhs[i] = <-Rhs_chan[i]
		dts[i] = float64(i) * deltat
	}

	return dts, Rhs
}
