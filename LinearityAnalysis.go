package WaveformAnalysis

import (
	"log"

	polyfit "gitlab.cern.ch/bnl-omega-go/polyfit"
)

func LinearityAnalysis(ts, wfs [][]float64, injection []float64, imax float64) (Amplitudes, INL, result []float64) {

	Amplitudes = make([]float64, len(wfs))
	Amplitudes_chan := make([]chan float64, len(wfs))

	INL = make([]float64, len(wfs))
	for i, wf := range wfs {
		Amplitudes_chan[i] = make(chan float64)

		go func(t, wf []float64, i int, amplitude_chan chan float64) {
			_, amplitude := FindMaximum(t, wf)
			amplitude_chan <- amplitude

			result := DoShapeAnalysis(t, wf, int64(i), "WF")
			log.Println(result)
		}(ts[i], wf, i, Amplitudes_chan[i])

	}

	for i := range wfs {
		Amplitudes[i] = <-Amplitudes_chan[i]
	}

	result = polyfit.Polyfit(injection, Amplitudes, 1, []float64{0, (Amplitudes[1] - Amplitudes[0]) / (injection[1] - injection[0])})

	Amax := result[0] + result[1]*imax

	for i, A := range Amplitudes {
		INL[i] = 100 * (A - (result[0] + result[1]*injection[i])) / Amax
	}

	return

}
